# Les Fabricants Website

## How to start

The website is using [hugo](https://gohugo.io).

### Prerequisites

Install `node`, `npm` and `hugo`.

### Init the project

```
git clone https://gitlab.com/lesfabricants/lesfabricants.re.git
cd lesfabricants.re
npm install
```

### Start the server

```
npm start
```