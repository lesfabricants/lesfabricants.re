import {
  Application,
  Controller,
} from "https://unpkg.com/@hotwired/stimulus@3.0.1/dist/stimulus.js";

const application = Application.start();

application.register(
  "mobilemenu",
  class extends Controller {
    static targets = ["nav", "open", "close"];

    toggle() {
      this.navTarget.classList.toggle("hidden");
      this.openTarget.classList.toggle("hidden");
      this.closeTarget.classList.toggle("hidden");
    }
  }
);
