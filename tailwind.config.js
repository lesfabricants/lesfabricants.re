module.exports = {
  content: ["./layouts/**/*.html"],
  theme: {
    extend: {
      typography: (theme) => ({
        "gray-100": {
          css: {
            color: theme("colors.gray.100"),
            ...["h1", "h2", "h3", "h4", "strong", "a"].reduce(
              (acc, tag) => ({
                ...acc,
                [tag]: {
                  color: theme("colors.gray.100"),
                },
              }),
              {}
            ),
          },
        },
        primary: {
          css: {
            a: {
              color: "#ac221e",
            },
          },
        },
      }),
      fontFamily: {
        display: ["Bebas Neue", "sans-serif"],
        body: ["Roboto", "sans-serif"],
        secondary: ["Montserrat", "sans-serif"],
      },
      colors: {
        primary: "#ac221e",
        "primary-light": "#F87D70",
        "primary-dark": "#760000",
        secondary: "#595959",
        "secondary-light": "#868686",
        "secondary-dark": "#303030",
        "primary-dark-text": "#d7915c",
      },
    },
  },
  plugins: [require("@tailwindcss/typography"), require("@tailwindcss/forms")],
};
