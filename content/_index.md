---
title: Les Fabricants, cave à vins à Saint-Denis de La Réunion
description:
  Cave et bar à vins à Saint-Denis de La Réunion. Plus de 400 références
  de vins naturels et conventionnels. Viandes maturée, fromage et charcuterie. Dégustation
  sur place et à emporter.
featured_image: "/images/devanture.jpg"
featured_image_description: "Devanture de la cave à vins Les Fabricants à Saint-Denis, La Réunion"
keywords:
  - cave à vins
  - Restaurant La Fabrique
  - Jehan Colson
  - vins naturels
  - Saint-Denis, La Réunion
---

Une sélection de vins naturels et conventionnels réunie par une équipe de passionnés du vin et de la nature, en collaboration avec des vignerons engagés.
