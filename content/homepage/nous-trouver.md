---
title: Nous trouver
weight: 2
header_menu: true
data:
  - name: appelez-nous
    value: "[0262 53 86 84](tel:0262538684)"
    icon: phone
  - name: venez
    value: 74 rue Pasteur — 97400 Saint-Denis
    icon: globe-alt
  - name: écrivez-nous
    value: "[lacave@lesfabricants.re](mailto:lacave@lesfabricants.re)"
    icon: at-symbol
  - name: Bar à Vins
    value: ouvert du **mercredi au samedi 10h/23h**
    icon: clock
  - name: Cave à Vins
    value: ouvert **lundi et mardi 10h/18h** et du **mercredi au samedi 10h/23h**
    icon: clock
---

Retrouvez-nous et partageons ensemble un moment enchanteur.
