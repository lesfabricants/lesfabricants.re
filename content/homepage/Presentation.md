---
title: Présentation
weight: 1
header_menu: false

---
Venez découvrir notre sélection de vins naturels et conventionnels dans notre cave à vins **Les Fabricants**.

Nos cavistes hors-pair, ont sélectionné pour vous plus de 400 références.

Vins naturels, bio, en biodynamie, pétillants naturels ou plus classiques… Laissez vous guider par nos sommeliers passionnés.

Un conseil, une dégustation, une initiation ? Nous avons à cœur d'apporter toute notre expertise à votre service.

Nous travaillons avec des vignerons de talent respectueux de la nature, du terroir et du consommateur.

Nous avons pensé à tout car **Les Fabricants** c'est aussi :

* des charcuteries & fromages triés sur le volet par notre chef Jehan Colson & [la fromagerie Au Bon Fromage à Etang-Salé](https://aubonfromage.re/?utm_source=lesfabricants.re).