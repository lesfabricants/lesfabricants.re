---
title: Le Vin Nature
description: Chez Les Fabricants, nous vous proposons du vin nature ou vin naturel.
menu:
  main:
    weight: 1

---
![Photo libre de droit de Domaine Viticole Avec Vignes banque d'images et  plus d'images libres de droit de Agriculture - iStock](https://img.freepik.com/photos-gratuite/grappes-raisins-rouges-suspendues-vigne-au-soleil_1401-396.jpg?size=626&ext=jpg)

## Notre philosophie

Notre sélection de vins est à tendance nature voire S.A.I.N.S. (Sans Adjonction d'Intrants Ni Sulfites).

Le vin nature n’est pas défini légalement.  
Il relève d’une démarche personnelle du vigneron : un projet, une philosophie.

Nos vignerons sont des vignerons de talent : des Vins Gourmands – Vivants – 100 % Raisin.

Loin des dogmes et des règles fixes, il repose néanmoins sur deux piliers :

### Le respect de la nature

Ceci par le biais d’une démarche biologique ou biodynamique en respectant l’écosystème de la Terre. Ces vignerons respectent leur terroir, préservent la mémoire du sol donc l’ADN du vin.

### Le respect du consommateur

En préférant une fermentation naturelle, douce et longue et une vinification sans produit rajouté, avec le moins de SO2 possible.  
Les vins gagnent ainsi en complexité aromatique, en texture en bouche et sont donc plus digestes.

Laissez-vous guider…