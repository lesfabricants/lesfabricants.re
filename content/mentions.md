---
title: Mentions Légales
description: Mentions légale du site https://lesfabricants.re
menu:
  footer:
    weight: 1

---
Pour toute question relative à ce site, vous pouvez nous contacter par le biais de notre page [contact](/#nous-contacter).

## Hébergement

Site hébergé par [Vercel](https://vercel.com/home).

Source disponible sur [Gitlab](https://gitlab.com/lesfabricants/website).

## Responsable légal

### Siège social - LES FABRICANTS

74 RUE PASTEUR
97400 SAINT-DENIS

### Siret - LES FABRICANTS

843 756 693 00027

### Nom commercial

LES FABRICANTS

### Enseigne

LES FABRICANTS

### Forme juridique

Société à responsabilité limitée